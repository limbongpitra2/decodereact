import React from "react";
import { createRoot } from "react-dom/client";
import ContactApp from "./components/ContactApp";
import  MyForm  from "./components/form";
import './styles/style.css'
const root = createRoot(document.getElementById('root'))
const myform = createRoot(document.getElementById('form'))

root.render(<ContactApp/>)
myform.render(<MyForm/>)